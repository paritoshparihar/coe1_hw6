function visitLink(path) {
	let visitCount = parseInt(localStorage.getItem(path)) || 0;
	localStorage.setItem(path, visitCount + 1);
  }

  function viewResults() {
	let results = {};
	for (let i = 0; i < localStorage.length; i++) {
	let key = localStorage.key(i);
	results[key] = localStorage.getItem(key);
	}
	let resultsList = document.createElement('ul');
	for (let key in results) {
	if (results !== 0) {
    let visitCount = results[key];
	let listItem = document.createElement('li');
	listItem.textContent = `You Visited ${key} - ${visitCount} times(s)`;
	resultsList.appendChild(listItem);
    }
	
	}
	let content = document.getElementById('content');
	content.appendChild(resultsList);
	
	localStorage.clear();
  }
  